package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        while (true) {
        System.out.println("Let's play round " + roundCounter);
        // System.out.println("Your choice (Rock/Paper/Scissors)?");
        String inputt = "";
        while (true) {
            inputt = readInput("Your choice (Rock/Paper/Scissors)?");
            if (inputt .equals("rock") || inputt .equals("paper") || inputt .equals("scissors")) {
                break;
            }
            System.out.println("I do not understand " + inputt + "." + " Could you try again?");
            
            
    }
        int human_input = 0;
        int pc = ThreadLocalRandom.current().nextInt(0, 3);

        if (inputt .equals("rock")) {
            human_input = 1;
        }
            
        else if (inputt .equals("paper")) {
            human_input = 2;
        }
        else if (inputt .equals("scissors")){
            human_input = 3;
        }
        
        // System.out.println(human_input);
        // System.out.println(pc);
        String[] rps = {"rock", "paper", "scissors"};

        String pc_choice = rps[pc];

        
        


        if (human_input == pc + 1) {
            System.out.println("Human chose " + inputt + ", computer chose " + pc_choice + ". It's a tie!");
        }

        else if (human_input == 1 && pc == 1) { //rock vs paper
            System.out.println("Human chose " + inputt + ", computer chose " + pc_choice + ". Computer wins!");
            computerScore++;
        }
        else if (human_input == 2 && pc == 0) { //paper vs rock
            System.out.println("Human chose " + inputt + ", computer chose " + pc_choice + ". Human wins!");
            humanScore++;
        }
        else if (human_input == 3 && pc == 0) { //scissors vs rock
            System.out.println("Human chose " + inputt + ", computer chose " + pc_choice + ". Computer wins!");
            computerScore++;
        }
        else if (human_input == 1 && pc == 2) { //rock vs scissors
            System.out.println("Human chose " + inputt + ", computer chose " + pc_choice + ". Human wins!");
            humanScore++;
        }
        else if (human_input == 3 && pc == 1) { //scissors vs paper
            System.out.println("Human chose " + inputt + ", computer chose " + pc_choice + ". Human wins!");
            humanScore++;
        }
        else if (human_input == 2 && pc == 2) { //paper vs scissors
            System.out.println("Human chose " + inputt + ", computer chose " + pc_choice + ". Computer wins!");
            computerScore++;
        }
        System.out.println("Score: human " + humanScore + ", computer " + computerScore);
        roundCounter++;

        String play_again = "";
        int p = 0;

        while (true) {
            play_again = readInput("Do you wish to continue playing? (y/n)?");
            if (play_again .equals("n") ) {
                p++;
                break;
            }
            else if (play_again .equals("y")) {
                break;
            }
            else {
                System.out.println("I do not understand " + play_again + ". " + "Could you try again?");
            }

        }
        if (p == 1) {
            System.out.println("Bye bye :)");
            break;
        }
    }
        // TODO: Implement Rock Paper Scissors
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
